FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=7.0.25-17292-1

RUN echo 'deb https://www.ui.com/downloads/unifi/debian stable ubiquiti' | sudo tee /etc/apt/sources.list.d/100-ubnt-unifi.list
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv 06E85760C0A52C50
RUN apt-get update
RUN apt-get install -y openjdk-8-jre-headless
RUN apt-get install -y jsvc
RUN apt-get download unifi=${VERSION}
RUN dpkg --ignore-depends=mongodb-server --ignore-depends=mongodb-10gen --ignore-depends=mongodb-org-server --ignore-depends=logrotate -i "unifi_${VERSION}_all.deb"

COPY start.sh /app/code/start.sh

RUN ln -sf /app/data/unifi /usr/lib/unifi/data
RUN ln -sf /tmp/logs /logs

ADD start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
