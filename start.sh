#!/bin/bash

set -eu

# https://stackoverflow.com/questions/54620708/configuring-unifi-controller-to-use-an-external-mongodb-database-possible-role

echo "=> Ensure directories"
mkdir -p /app/data/unifi /tmp/logs
chown cloudron:cloudron -R /app/data /tmp/logs

if ! grep "db.mongo.local" /app/data/unifi/system.properties; then
    echo "=> Initial mongodb settings"
    echo "db.mongo.local=false" >> /app/data/unifi/system.properties
    echo "db.mongo.uri=${CLOUDRON_MONGODB_URL}" >> /app/data/unifi/system.properties
    echo "statdb.mongo.uri=${CLOUDRON_MONGODB_URL}" >> /app/data/unifi/system.properties
    echo "unifi.db.name=${CLOUDRON_MONGODB_DATABASE}" >> /app/data/unifi/system.properties
fi

echo "=> Ensuring mongodb settings"
sed -e "s|db.mongo.local=.*|db.mongo.local=false|g" \
    -e "s|db.mongo.uri=.*|db.mongo.uri=${CLOUDRON_MONGODB_URL}|g" \
    -e "s|statdb.mongo.uri=.*|statdb.mongo.uri=${CLOUDRON_MONGODB_URL}|g" \
    -e "s|unifi.db.name=.*|unifi.db.name=${CLOUDRON_MONGODB_DATABASE}|g" \
    -i /app/data/unifi/system.properties

echo "=> Start server"
/usr/bin/jsvc -user cloudron -nodetach -debug -home /usr/lib/jvm/java-8-openjdk-amd64/ -cp /usr/share/java/commons-daemon.jar:/usr/lib/unifi/lib/ace.jar -pidfile /tmp/unifi.pid -outfile "&2" -errfile "&1" com.ubnt.ace.Launcher start
