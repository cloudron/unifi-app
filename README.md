# Unifi Controller Cloudron App

This repository contains the Cloudron app package source for Unifi Controller.

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?com.unifi.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.unifi.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd unifi-app

cloudron build
cloudron install
```

